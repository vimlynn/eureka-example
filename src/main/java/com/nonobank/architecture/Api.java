package com.nonobank.architecture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

/**
 * Created by weilin.wang on 2018/6/6
 */
@Controller
@Service
public class Api {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("test")
    @ResponseBody
    public Object test(){
        String str = restTemplate.getForObject("http://eseal-app/monitor", String.class);
        return str;
    }

    @RequestMapping("test2")
    @ResponseBody
    public Object test2(){
        String str = restTemplate.getForObject("eseal-app/monitor", String.class);
        return str;
    }

    @RequestMapping("test3")
    @ResponseBody
    public Object test3(){
        String str = restTemplate.getForObject("http://eseal-app/eseal-app/monitor", String.class);
        return str;
    }

    @RequestMapping("test4")
    @ResponseBody
    public Object test4(){
        String str = restTemplate.getForObject("http://imageCode-app/monitor", String.class);
        return str;
    }

    @RequestMapping("hello")
    @ResponseBody
    public Object hello(){
        System.out.println(restTemplate);
        return "hello";
    }
}