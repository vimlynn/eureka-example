package com.nonobank.architecture.extend;

public class Parent {

    String name = "Parent";

    public void m1() {
        System.out.println("m1");
    }

    public void m12() {
        this.m1();
        System.out.println();
        System.out.println("" + this.name);
    }
}
