package com.nonobank.architecture.extend;

public class Son extends Parent {

    String name = "Son";

    public void print(){
        System.out.println("" + this.name);
    }

    @Override
    public void m1() {
        System.out.println("son m1");
    }

    @Override
    public void m12() {
//        super.name = "12312";
        super.m12();
    }
}
